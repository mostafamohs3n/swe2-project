<?php

namespace App\Repositories\Eloquent;

use App\Post;
use App\User;
use App\Like;
use App\Repositories\Contracts\PostRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentPostRepository extends AbstractRepository implements PostRepository
{

    private $model;

    public function __construct(Post $model){
        $this->model = $model;
    }

    public function entity()
    {
        return Post::class;
    }

    public function getAll(){
        return User::findOrFail(auth()->id())->post;
        // return $this->model->all();
    }

    public function getById($id){
        return $this->model->find($id);
    }

    public function create(array $attributes){
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes){
        $post =  $this->model->findOrFail($id);
        $post->update($attributes);
    }

    public function delete($id){
        Like::where('post_id', $id)->delete();
        $this->model->find($id)->delete();
        return true;
    }
}
