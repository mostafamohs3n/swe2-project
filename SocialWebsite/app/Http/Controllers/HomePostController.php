<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Like;
use App\User;
use DB;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Contracts\PostRepository;

class HomePostController extends Controller
{
    private $post;


    public function __construct(PostRepository $post){
      $this->middleware('verified');
      $this->post = $post;
    }
    // store = action
    // create = view
    public function create(){
        return view('post/createpost');
    }
    public function store(Request $request){
        $request->validate([
            'body' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|required',
            ]);
        $this->post->create(['user_id' => Auth::user()->id,'body' => $request->input('body'), 'image' => $new_name ] );
        return redirect(route('home'));
    }

    public function index()
    {
        $arr = $this->post->getAll();
        return view('home', $arr);
    }
    public function delete(Request $request)
    {
        $this->post->delete();
        return redirect(route('home'));

    }
    public function edit(Request $request)
    {
        $this->post->update($request->input('post_id'), [$request->input('body')] );
        return redirect(route('home'));

    }
    public function show(Request $request)
    {
        $post = $this->post->getById($request->post_id);
        return view('post.edit',compact('post'));
    }
}
