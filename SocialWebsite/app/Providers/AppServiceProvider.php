<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Custom\Settings;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      if ($this->app->environment() == 'local') {
        $this->app->register('Kurt\Repoist\RepoistServiceProvider');
      }


      $this->app->bind(
            'App\Repositories\Contracts\PostRepository',
            'App\Repositories\Eloquent\EloquentPostRepository'
        );


    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->share('settings', Settings::getInstance());
        \Schema::enableForeignKeyConstraints();
    }
}
